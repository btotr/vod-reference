function VideoPlayer(videoElement, src) {
    this.videoElement = videoElement;
    this.node = this.videoElement.parentNode;
    
    var self = this
    
    // add progressbar
    this.progressbar = this.createProgressbar();
    
    // video events
    videoElement.addEventListener("error", this.showError.bind(this));
    videoElement.addEventListener("ended", this.close.bind(this));
    videoElement.addEventListener("timeupdate", this.updateState.bind(this));
    videoElement.addEventListener("durationchange", this.updateState.bind(this));

    this.play(src);
}

VideoPlayer.prototype.updateState = function updateState(e) {
    // set the relative position
    this.progressbar.setAttribute("value", e.target.currentTime/e.target.duration*100)
}

VideoPlayer.prototype.play = function play(src) {
    if (src) this.videoElement.src = src;
    this.videoElement.play();
    this.reset();
}

VideoPlayer.prototype.close = function close() {
    window.close();
}

VideoPlayer.prototype.pause = function pause(src){
    if (parseInt(this.node.getAttribute("data-speed")) > 1) {
        this.reset()
        return
    }
    this.videoElement.pause();
    this.node.classList.add("show-progress");
    this.node.classList.add("pause");
}

VideoPlayer.prototype.reset = function reset() {
    this.node.classList.remove("pause")
    this.node.setAttribute("data-speed", 1);
    this.node.classList.remove("show-progress");
    this.node.classList.remove("backward");
    this.node.classList.remove("forward");
}

VideoPlayer.prototype.backward = function backward(){
    this.forward(true)
}

VideoPlayer.prototype.getSpeed = function getSpeed(backward){
    var playRate = this.videoElement.playbackRate;
    var multiplier = 2;
    if (playRate < 0.5) multiplier = 0.5;
    if (playRate == -0.5) multiplier = -2;
    if (backward) {
        if (playRate > 0.5) multiplier = 0.5; 
        if (playRate < 0.5) multiplier = 2;
        if (playRate == 0.5) multiplier = -2;
    }
    return playRate*multiplier
}

VideoPlayer.prototype.forward = function forward(backward) {
    // cancel timer fade normal speed
    try{ clearTimeout(normalSpeed) } catch(ignore) {}
    var speed = this.getSpeed(backward);

    if (speed == 1) {
        normalSpeed = setTimeout(function(){
            this.node.classList.add("resume");
            setTimeout(function(){
                this.node.classList.remove("resume");
                this.reset() 
            }.bind(this),3200);
                       
        }.bind(this), 1500)
    }

    if (speed > 16 || speed < -16) {
        return
    }
    
    this.node.setAttribute("data-speed", Math.abs(speed));
    this.videoElement.playbackRate = speed;
    this.node.classList.add("show-progress");
    if (speed > 0.5) {
        this.node.classList.remove("backward");
        this.node.classList.add("forward");
        return
    }
    this.node.classList.add("backward");
}

VideoPlayer.prototype.createProgressbar = function createProgressbar(e) {
    var progressbar = document.createElement("progress")
    progressbar.setAttribute("max", 100);
    this.node.appendChild(progressbar);
    
    // older cloudtv version doesn't support progress element
    if('position' in progressbar) return progressbar;

    // progress polyfill
    var pollyfillBar = document.createElement("div");
    pollyfillBar.classList.add("progress-bar");
    progressbar.appendChild(pollyfillBar);
    
    var pollyfillValue = document.createElement("div");
    pollyfillValue.classList.add("progress-value");
    pollyfillBar.appendChild(pollyfillValue);
    
    var observer = new WebKitMutationObserver(function (mutations) {
        mutations.forEach(function attrModified(mutation) {
            if(mutation.attributeName == "value"){
                pollyfillValue.style.width = mutation.target.getAttribute('value') + "%";
            }
        });
    });
    observer.observe(progressbar, { attributes: true, subtree: false });
    
    return progressbar
}

VideoPlayer.prototype.showError = function showError() {
    switch (this.videoElement.error) {
        case 1: message = "aborted"; break;
        case 2: message = "network error"; break;
        case 3: message = "decode error"; break;
        case 4: message = "source not supported"; break;
        default: 
            message = "unexpected error";
            if (this.videoElement.hasAttribute("AVNErrorString")){
                // AVNErrorString is a cloudtv specific error string with additinal information
                message = this.videoElement.getAttribute("AVNErrorString");
                var error = (/^.*=[0-9]*/.exec(message) || [""])[0];
                message = message.substr(error.length);
                // report error
                console.log("report_event:" + JSON.stringify({"vod-error": error, "message": message}));
            } 
    }
    // create a simple error dialog
    var errorDialog = document.createElement("div");
    errorDialog.setAttribute("id", "error-dialog");
    errorMessage = document.createElement("p");
    errorMessage.textContent = message;
    errorDialog.appendChild(errorMessage);
    document.body.appendChild(errorDialog);
}

window.addEventListener("load", function(){
    var videoPlayer = new VideoPlayer(document.getElementsByTagName("video")[0],"vod:concurrent/Amelia_Island_30sec_SD");
    //this.videoPlayer = new VideoPlayer(document.getElementsByTagName("video")[0],"http://webservices.rendercast.com/media/mute.webm");
    // control the video player
    document.addEventListener("keydown", function(e){
        if (e.keyCode == "13") {
            if(videoPlayer.node.classList.contains("pause")) {
                videoPlayer.play();
                return
            }
            videoPlayer.pause();
        }
        
        if (e.keyCode == "39") {
            videoPlayer.forward();
        }
        
        if (e.keyCode == "37") {
            videoPlayer.backward();
        }
        
        if (e.keyCode == "8") {
            videoPlayer.close();
            e.preventDefault();
        }

    })        
            
            
})



